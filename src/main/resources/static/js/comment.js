$(function(){
  $('.read-comment').hide();
  $('.write-comment').hide();

  $('.comment-read').on('click',function(){
    // クリックした要素の ID と同じクラスのセクションを表示
    $('.read-comment').toggle();
  });

  $('.comment-write').on('click', function(){
	  $('.write-comment').toggle();
  })
});