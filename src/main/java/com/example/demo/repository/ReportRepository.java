package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

/*
 * @Repository:DBとのやり取りをおこなうクラスに付与
 * JpaRepositoryを継承することで、DBの操作をおこなうメソッド類を使うことができる
 * JpaRepository<エンティティ, IDタイプ>
 * エンティティは、捜査対象のテーブル
 * IDタイプは、主キーのデータ型
 */
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{
	List<Report> findByCreatedDateBetweenOrderByUpdatedDateDesc(Date start, Date end);
	List<Report> findAllByOrderByUpdatedDateDesc();

}
