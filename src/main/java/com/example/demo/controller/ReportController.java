package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ReportController {

	/*
	 * @Autowired:フィールドに付与。フィールドに合致するオブジェクトを自動的にインジェクションする
	 */
	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	/*
	 *  投稿内容表示画面
	 *  @GetMapping:Getリクエストに対する処理をおこなう
	 */
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();

		// 投稿を全件取得
		List<Report> contentData = reportService.findAll();
		// コメントを全件取得
		List<Comment> commentData = commentService.findAll();
		// 新規コメント用の空のentityを準備
		Comment comment = new Comment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管
		mav.addObject("comments", commentData);
		// 準備した空のentityを保管
		mav.addObject("commentModel", comment);
		return mav;
	}
	/*
	 * 新規投稿画面
	 * @GetMapping:リクエストURLが「/new」だった場合のGetリクエストに対する処理をおこなう
	 */
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	/*
	 * 投稿処理
	 * @PostMapping:リクエストURLが「/add」だった場合のPostリクエストに対する処理をおこなう
	 * @ModelAttribute:SpringFrameワークのModelと、フレームワークなどに関係していないPOJO（Plain Old Java Object）を紐づける
	 */
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.save(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 投稿内容に不随するコメントを削除
		commentService.deleteByReportId(id);
		// 投稿されている内容を削除
		reportService.delete(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿編集画面
	@GetMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用に投稿内容を格納したentityを準備
		Report report = reportService.findOne(id);
		// 画面遷移先を指定
		mav.setViewName("/update");
		// 編集対象の投稿内容を格納したentityを保管
		mav.addObject("formUpdateModel", report);
		return mav;
	}
	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formUpdateModel") Report report) {
		//更新対象のidをEntityにセット
		report.setId(id);
		report.setUpdated_date(new Date());
		// 編集した投稿を更新
		reportService.save(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
	// 絞り込み処理
	@GetMapping("/search")
	public ModelAndView searchReport(@RequestParam("start") String startDate, @RequestParam("end") String endDate) throws ParseException {
		ModelAndView mav = new ModelAndView();

		// 絞り込み処理
		List<Report> contentData = reportService.findSome(startDate, endDate);

		// コメントを全件取得
		List<Comment> commentData = commentService.findAll();
		// 新規コメント用の空のentityを準備
		Comment comment = new Comment();

		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管
		mav.addObject("comments", commentData);
		// 準備した空のentityを保管
		mav.addObject("commentModel", comment);
		return mav;
	}

}
