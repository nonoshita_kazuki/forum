package com.example.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class CommentController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// コメント処理
	@PostMapping("/comment/")
	public ModelAndView comment(@RequestParam("reportId") Integer reportId, @ModelAttribute("commentModel") Comment comment) {
		// 投稿IDをコメントentityにセット
		comment.setReport_id(reportId);
		// 更新日時を現在日時に設定
		comment.setUpdated_date(new Date());
		commentService.save(comment);

		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@GetMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用に投稿内容を格納したentityを準備
		Comment comment = commentService.findOne(id);
		// 画面遷移先を指定
		mav.setViewName("/updateComment");
		// 編集対象の投稿内容を格納したentityを保管
		mav.addObject("formUpdateModel", comment);
		return mav;
	}

	// コメント編集処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formUpdateModel") Comment comment) {
		//更新対象のidをEntityにセット
		comment.setId(id);
		comment.setUpdated_date(new Date());

		// 編集した投稿を更新
		commentService.save(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// 投稿されている内容を削除
		commentService.delete(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}



}
