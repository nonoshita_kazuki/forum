package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	@Autowired
	ReportService reportService;

	// 投稿に対するコメントの全件取得
	public List<Comment> findAll(){
		return commentRepository.findAll();

	}

	// コメントの追加
	public void save(Comment comment) {
		commentRepository.save(comment);
		// 投稿の更新日時も更新
		Report report = reportService.findOne(comment.getReport_id());
		report.setUpdated_date(comment.getUpdated_date());
		reportService.save(report);
	}

	// コメント１件取得
	public Comment findOne(int id) {
		return commentRepository.getById(id);
	}

	// コメントの削除
	public void delete(int id) {
		commentRepository.deleteById(id);
	}

	// 投稿内容に付随するコメントの削除
	public void deleteByReportId(int id) {
		commentRepository.deleteByReportId(id);
	}
}
