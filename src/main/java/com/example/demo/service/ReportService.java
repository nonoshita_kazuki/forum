package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAll() {
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}
	// レコード追加・更新
	public void save(Report report) {
		reportRepository.save(report);
	}
	// レコード削除
	public void delete(int id) {
		reportRepository.deleteById(id);
	}
	// レコード１件取得
	public Report findOne(int id) {
		return reportRepository.getById(id);
	}

	// 日付指定でレコードを取得
	public List<Report> findSome(String startDate, String endDate) throws ParseException{
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = sdFormat.parse("2022-03-01 00:00:00");
		Date end = new Date();
		if(startDate != "") {
			startDate += " 00:00:00";
			start = sdFormat.parse(startDate);
		}
		if(endDate != ""){
			endDate += " 23:59:59";
			end = sdFormat.parse(endDate);
		}
		//検索処理
		return reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(start, end);
	}
}
