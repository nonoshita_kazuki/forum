package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @Entity:データの入れ物となるEntityクラスであることを示す
 * @table:Entityクラスと対応するデータベースのテーブル名を指定する
 */
@Entity
@Table(name = "report")
public class Report {

	/*
	 * @Id:Entityクラス内で定義した主キーに付与する
	 * @Column:データベースのテーブルのカラム名に相当するメンバ変数に付与する
	 * @GeneratedValue:主キーの自動生成。strategy属性を付与することで、生成する方法を指定することができる
	 */
	@Id
	@Column(name = "id", insertable = true, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String content;

	@Column(name = "created_date", insertable = false, updatable = false)
	private Date createdDate;

	@Column(name = "updated_date", insertable = false, updatable = true)
	private Date updatedDate;

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date created_date) {
		this.createdDate = created_date;
	}
	public Date getUpdated_date() {
		return updatedDate;
	}
	public void setUpdated_date(Date updated_date) {
		this.updatedDate = updated_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}